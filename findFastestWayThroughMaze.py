import sys
totalTests = 0
passedTests = 0
currentFastestRoute = 0

# Direction: 0 = top, 1 = right, 2 = down, 3 = left
# -1 in a grid spot means we have used that position before
#
# Description:
#   1. If possible, move right
#   2. Repeat step 1 until impossible
#   3. If possible, move down
#   4. Repeat steps 1-3 until you cannot move right or down
#   5. If possible, move left
#   6. Repeat steps 1-5 until you cannot move right, down, or left
#   7. If possible, move up
#   8. Repeat steps 1-7 until you cannot move right, down, left, or right
#   9. If you cannot move any direction, go back to previous node and try next
#       available direction.
#
# Note: 
#   - Impossible means you cannot change anymore nodes from 1 to 0.
#   - Impossible also means the node was entered previously.
#   - Each move also checks to see if current x and y are the bottom right. If 
#     they are, store in global variable if it is current fastest route. The
#     script then moves to previous node and try next available direction until
#     it cannot move or it is at the 'exit'. Repeat until x and y both equal 0,
#     and all directions have been tried.
#   - If fastest possible route (L + W - 1) has been found, we do not check any
#     more routes.
def solution(map):
    move(0, 1, 1, 0, 0, len(map[0]) - 1, len(map) - 1, map)
    return currentFastestRoute

def move(directionCameFrom, movesLeft, totalMovesMadeSoFar, x, y, w, h, grid):
    global currentFastestRoute
    gridCopy = copyList(grid)

    # If fastest possible route has been found
    if currentFastestRoute == (w + h + 1):
        return

    # If current route is already more moves than a previously found route, just return
    if currentFastestRoute > 0 and totalMovesMadeSoFar >= currentFastestRoute:
        return

    # If we have completed the maze
    if x == w and y == h:
        if currentFastestRoute == 0 or totalMovesMadeSoFar < currentFastestRoute:
            currentFastestRoute = totalMovesMadeSoFar
        return
    
    if directionCameFrom != 1 and x < w and grid[y][x + 1] == 0:
        # Move right
        grid[y][x + 1] = -1
        move(3, movesLeft, totalMovesMadeSoFar + 1, x + 1, y, w, h, grid)
        grid = copyList(gridCopy)
    elif directionCameFrom != 1 and x < w and grid[y][x + 1] == 1 and movesLeft == 1:
        # Move right
        grid[y][x + 1] = -1
        move(3, 0, totalMovesMadeSoFar + 1, x + 1, y, w, h, grid)
        grid = copyList(gridCopy)

    # If fastest possible route has been found
    if currentFastestRoute == (w + h + 1):
        return

    if directionCameFrom != 2 and y < h and grid[y + 1][x] == 0:
        # Move down
        grid[y + 1][x] = -1
        move(0, movesLeft, totalMovesMadeSoFar + 1, x, y + 1, w, h, grid)
        grid = copyList(gridCopy)
    elif directionCameFrom != 2 and y < h and grid[y + 1][x] == 1 and movesLeft == 1:
        # Move down
        grid[y + 1][x] = -1
        move(0, 0, totalMovesMadeSoFar + 1, x, y + 1, w, h, grid)
        grid = copyList(gridCopy)

    # If fastest possible route has been found
    if currentFastestRoute == (w + h + 1):
        return
        
    if directionCameFrom != 3 and x > 0 and grid[y][x - 1] == 0:
        # Move left
        grid[y][x - 1] = -1
        move(1, movesLeft, totalMovesMadeSoFar + 1, x - 1, y, w, h, grid)
        grid = copyList(gridCopy)
    elif directionCameFrom != 3 and x > 0 and grid[y][x - 1] == 1 and movesLeft == 1:
        # Move left
        grid[y][x - 1] = -1
        move(1, 0, totalMovesMadeSoFar + 1, x - 1, y, w, h, grid)
        grid = copyList(gridCopy)

    # If fastest possible route has been found
    if currentFastestRoute == (w + h + 1):
        return
        
    if directionCameFrom != 0 and y > 0 and grid[y - 1][x] == 0:
        # Move up
        grid[y - 1][x] = -1
        move(2, movesLeft, totalMovesMadeSoFar + 1, x, y - 1, w, h, grid)
        grid = copyList(gridCopy)
    elif directionCameFrom != 0 and y > 0 and grid[y - 1][x] == 1 and movesLeft == 1:
        # Move up
        grid[y - 1][x] = -1
        move(2, 0, totalMovesMadeSoFar + 1, x, y - 1, w, h, grid)
    return

def copyList(originalList):
    newList = []
    for subList in originalList:
        newList.append(subList[:])
    return newList

def test1():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0]]
    expected = 28
    print 'Test 1'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test2():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 1],
        [0, 1, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0]]
    expected = 10
    print 'Test 2'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test3():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 1, 1, 0],
        [0, 0, 0, 1],
        [1, 1, 0, 0],
        [1, 1, 1, 0]]
    expected = 7
    print 'Test 3'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test4():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 1, 1],
        [0, 1, 0, 1, 1],
        [0, 0, 0, 0, 0]]
    expected = 11
    print 'Test 4'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test5():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0],
        [1, 1, 1, 1, 0],
        [1, 1, 1, 1, 0],
        [1, 1, 1, 0, 0],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 1, 1],
        [1, 1, 0, 0, 0]]
    expected = 15
    print 'Test 5'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test6():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0]]
    expected = 14
    print 'Test 6'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test7():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0],
        [0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0],
        [1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0],
        [1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    expected = 98
    print 'Test 7'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test8():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0]]
    expected = 5
    print 'Test 8'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test9():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 1, 0, 0]]
    expected = 5
    print 'Test 9'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual
    print 'Passed: ', expected == actual
    print '\r\n'

def test10():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 1, 1, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 1, 1, 0]]
    expected = 22
    print 'Test 10'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test11():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1],
        [0, 0, 1, 0, 0, 0, 0],
        [0, 1, 0, 1, 1, 1, 0],
        [0, 1, 0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0]]
    expected = 14
    print 'Test 11'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test12():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 1, 0],
        [1, 1, 0, 1, 1, 1, 0],
        [1, 1, 0, 0, 0, 0, 0],
        [1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 1, 1, 1, 1],
        [1, 1, 0, 0, 0, 0, 0]]
    expected = 15
    print 'Test 12'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test13():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 1, 0],
        [1, 1, 0],
        [0, 0, 0]]
    expected = 5
    print 'Test 13'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test14():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 1, 1],
        [1, 1, 0],
        [0, 1, 1],
        [0, 0, 0]]
    expected = 6
    print 'Test 14'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test15():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 1, 1, 1, 1],
        [0, 1, 1, 1, 1],
        [0, 1, 1, 1, 0],
        [1, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 0, 1, 0],
        [0, 1, 0, 1, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0],
        [1, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 0, 1, 1],
        [0, 1, 0, 0, 1],
        [0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0]]
    expected = 34
    print 'Test 15'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test16():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 1, 1, 0],
        [1, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0],
        [0, 1, 1, 0, 1, 1, 1],
        [0, 1, 1, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1],
        [0, 1, 1, 0, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0]]
    expected = 27
    print 'Test 16'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test17():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0]]
    expected = 2
    print 'Test 17'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test18():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0],
        [0]]
    expected = 2
    print 'Test 18'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test19():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 1, 0, 0, 0],
        [0, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 0, 0],
        [0, 1, 1, 1, 1, 0, 1],
        [0, 1, 1, 1, 1, 0, 0],
        [0, 1, 1, 1, 1, 1, 0],
        [0, 1, 1, 1, 1, 1, 0],
        [0, 0, 0, 0, 0, 0, 0]]
    expected = 14
    print 'Test 19'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def test20():
    global currentFastestRoute,totalTests,passedTests
    totalTests = totalTests + 1
    currentFastestRoute = 0
    testData = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]
    expected = 39
    print 'Test 20'
    print 'Input: ' 
    printMatrix(testData)
    print 'Expected: ', expected 
    actual = solution(testData)
    passedTests = passedTests + 1 if expected == actual else passedTests
    print 'Actual: ', actual 
    print 'Passed: ', expected == actual 
    print '\r\n'

def printMatrix(matrix):
    for i in range(len(matrix)):
        for j in matrix[i]:
            sys.stdout.write(str(j) + ' ')
        print('')

# The reason there are so many tests is because foo.bar was failing on one of its tests.
# The test was [hidden] so I did not know what it was. I ended up fixing it with test 20.
# I think it was just taking too long that their test would just timeout. So I added the
# if statement that does not run anymore scenarios if we already have found fastest 
# possible route. This seemed to fix that test.
test1()
test2()
test3()
test4()
test5()
test6()
test7()
test8()
test9()
test10()
test11()
test12()
test13()
test14()
test15()
test16()
test17()
test18()
test19()
test20()
print 'Total Tests: ', totalTests
print 'Passed Tests: ', passedTests