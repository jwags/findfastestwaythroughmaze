## Find the Fastest Route Through a Maze

Given a grid, find the fastest way from the top left most node to the bottom right most node. 0s are nodes you can enter, 1s are nodes you cannot enter. You can change one impassible node (1) to a passible node (0). The solution should return the least amount of moves required.


Example:

Input: 

~~~~
grid = [
  [0, 1, 1, 0],
  [0, 0, 0, 1],
  [1, 1, 0, 0],
  [1, 1, 1, 0]]
~~~~
    

Output: 7